package com.example.evento;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SegundaActividad extends AppCompatActivity {

    private TextView tv_saludo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda_actividad);

        tv_saludo = findViewById(R.id.tv_saludo);

        Intent intent = getIntent();

        String nombre = intent.getStringExtra("nombre");

        tv_saludo.setText("Bienvenido " + nombre + " que gusto verte nuevamente");
    }
}