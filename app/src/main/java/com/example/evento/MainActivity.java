package com.example.evento;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btn_evento;
    private TextView tv_mensaje;
    private EditText pt_nombre;
    private Button btn_nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_evento = findViewById(R.id.btn_evento);
        tv_mensaje = findViewById(R.id.tv_mensaje);
        pt_nombre = findViewById(R.id.pt_nombre);
        btn_nombre = findViewById(R.id.btn_nombre);

        btn_nombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = pt_nombre.getText().toString();
                Intent intent = new Intent(MainActivity.this, SegundaActividad.class);

                intent.putExtra("nombre", nombre);

                startActivity(intent);
            }
        });


        btn_evento.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                tv_mensaje.setText("Felicidades, has creado tu primer Evento!!");
            }
        });

    }
}